# com-l2fprod-common-annotations
[![Build Status](https://travis-ci.org/ZenHarbinger/l2fprod-common-annotations.svg?branch=master)](https://travis-ci.org/ZenHarbinger/l2fprod-common-annotations)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.tros/l2fprod-common-annotations/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.tros/l2fprod-common-annotations/)
[![License](https://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Provides annotations that can be used by the [l2fprod Properties Editor](https://github.com/ZenHarbinger/l2fprod-properties-editor).
